<?php

/**
 * @file views_group_limit.inc
 * Our handler.
 */
class views_group_limit_views_plugin_style extends views_plugin_style_list {

  /**
   * Overrides parent::render_grouping().
   *
   * Provide an array walk on $sets to use group_limit_recurisve
   * to limit rows
   */
   function render_grouping($records, $groupings = array(), $group_rendered = NULL) {
    
    $sets = parent::render_grouping($records, $groupings, $group_rendered);
    if ($this->display->display_options['group_limit_settings']){
      // Apply the offset and limit.
      array_walk($sets, array($this, 'group_limit_recursive'));
    }
    return $sets;
  }

  /**
   * Recursively limits the number of rows in nested groups.
   *
   * @param array $group_data
   *   A single level of grouped records.
   *
   * @param mixed $key
   *   The key of the array being passed in. Used for when this function is
   *   called from array_walk() and the like. Do not set directly.
   *
   * @params int $level
   *   The current level we are gathering results for. Used for recursive
   *   operations; do not set directly.
   *
   * @return array
   *   An array with a "rows" property that is recursively grouped by the
   *   grouping fields.
   */
  function group_limit_recursive(&$group_data, $key = NULL, $level = 0) {
    //$settings = $this->views_group_limit_settings($level);
    $settings = $this->display->display_options['group_limit_settings'];
    $default_limit = $settings[0]['grouping-limit'];
    $default_offset = $settings[0]['grouping-offset'];

    // Slice up the rows according to the offset and limit.
    $group_data['rows'] = array_slice($group_data['rows'], 
      $default_offset, 
      $default_limit, TRUE);
   
    // For each row, if it appears to be another grouping, recurse again.
    foreach ($group_data['rows'] as &$data) {
      if (is_array($data) && isset($data['group']) && isset($data['rows'])) {
        $this->group_limit_recursive($data, NULL, $level + 1);
      }
    }
  }
}


class views_group_limit_plugin_display_extender extends views_plugin_display_extender {

  /**
   * Overrides parent::options_form().
   *
   * Provide for form to set group limits for each views
   * style with grouping
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    foreach ($form['style_options']['grouping'] as $index => $info) { 
      // Defined settings in the view display object
      $settings = $this->display->options['group_limit_settings'];

      $default_limit = $settings[$index]['grouping-limit'];
      $default_offset = $settings[$index]['grouping-offset'];
      
      // Form items
      $form['style_options']['grouping'][$index]['grouping-limit-fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => check_plain(t('Limit for grouping field number .!num', array('!num' => $index + 1),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#states' => array(
          'invisible' => array(
            "select[name=\"style_options[grouping][{$index}][field]\"]" => array('value' => ''),
            )
          ),
      );
      $form['style_options']['grouping'][$index]['grouping-limit-fieldset']['grouping-limit'] = array(
          '#type' => 'textfield',
          '#title' => t('Items to display:'),
          '#default_value' => isset($default_limit) ? $default_limit : 0,
          '#size' => 6,
          '#element_validate' => array('grouping_validate'),
          '#description' => t('The number of rows to show under each grouping header (only works when "Items to display" in the main view is set to unlimited).'),
      );
      $form['style_options']['grouping'][$index]['grouping-limit-fieldset']['grouping-offset'] = array(
          '#type' => 'textfield',
          '#title' => t('Offset:'),
          '#default_value' => isset($default_offset) ? $default_offset : 0,
          '#size' => 6,
          '#element_validate' => array('grouping_validate'),
          '#description' => t('The row to start on (<em>0</em> means it will start with first row, <em>1</em> means an offset of 1, and so on).'),
        );
    }

  }

  /**
   * Saves form values into views display object options.
   */
  function options_submit(&$form, &$form_state) { 
    parent::options_submit($form, $form_state);

    $group = $form_state['values']['style_options']['grouping'];
    
    $groups = array();
    foreach ($group as $index) {
      $groups[] = $index['grouping-limit-fieldset'];
    }

    $this->display->set_option('group_limit_settings', $groups);
  }



}

/**
 * Validate the added form elements.
 */
function grouping_validate($element, &$form_state) {
  // Checking to see if numeric.
  if (!is_numeric($element['#value'])) {
    form_error($element, t('%element must be numeric.', array('%element' => $element['#title'])));
  }

  // Checking for negative values.
  if ($element['#value'] < 0) {
    form_error($element, t('%element cannot be negative.', array('%element' => $element['#title'])));
  }
}
