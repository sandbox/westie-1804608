<?php

/**
 * @file views_group_limit.views.inc
 * Just describing our Views style plugin; nothing much to see here.
 */

/**
 * Implements hook_views_plugins().
 */
function views_group_limit_views_plugins() {
  $path = drupal_get_path('module', 'views_group_limit');
  $plugins = array(); 
  $plugins['display_extender']['views_group_limit'] = array( 
    'title' => t('Extender for apply a limit to a views group'), 
    'help' => t('Allows you to set a limit on grouped fields.'), 
    'path' => $path, 
    'handler' => 'views_group_limit_plugin_display_extender',
  ); 
  return $plugins; 
}